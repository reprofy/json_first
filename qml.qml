import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Window 2.3
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.3
//import Qt3D.Input 2.0

ApplicationWindow {
    title: qsTr("Choose your lesson")
    width: 480
    height: 240
    visible: true
    id: mainWindow
    RowLayout {
        id: rowL
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        Text {
            Layout.topMargin: 5
            Layout.bottomMargin: 5
            id: actionInvite
            anchors.left: parent.left
            text: qsTr("Choose lesson:")
        }
        ComboBox {
            Layout.topMargin: 5
            Layout.bottomMargin: 5
            id: firstLangButton
            anchors.right: orderButton.left
            model: ["en","de", "ru"]
            implicitWidth: 80
            Layout.leftMargin: 25
            anchors.rightMargin: 3
        }
        Button {
            Layout.topMargin: 5
            Layout.bottomMargin: 5
            id: orderButton
            anchors.right: secondLangButton.left
            text: "\u21C6"
            implicitWidth: 60
            Layout.leftMargin: 25
            anchors.rightMargin: 3
        }
        ComboBox {
            Layout.topMargin: 5
            Layout.bottomMargin: 5
            id: secondLangButton
            anchors.right: parent.right
          //  width: 20
            model: ["en","de", "ru"]
            implicitWidth: 80
            Layout.leftMargin: 25
            anchors.rightMargin: 5
        }

    }
    ScrollView {
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.top: rowL.bottom
        focus: true

        Keys.onDownPressed: listView.incrementCurrentIndex()
        Keys.onUpPressed: listView.decrementCurrentIndex()
        ListView {
            id: listView
            model: myModel
            clip: true
            keyNavigationEnabled: true
            highlightFollowsCurrentItem: true
            Component {
                id: listItem
                Rectangle {
                    id: compRect
                    property bool moreLessVisibility: false
                    height: cl.height > buttonsLayout.height ? cl.height : buttonsLayout.height
                    width: parent.width
                    color: "transparent"
                    border.width: 0.5
                    border.color: "blue"
                    radius: 5
                    z: 5
                    MouseArea {
                        anchors.fill: parent
                        propagateComposedEvents: true
                        onClicked: {listView.currentIndex = index; mouse.accepted=false}

                    }
                    RowLayout{
                        id: rl
                        ColumnLayout {
                            id: picsLt
                        Image {
                            id: iconImg
                            //anchors.left: parent.left
                            verticalAlignment: Image.AlignHCenter
                            Layout.leftMargin: 7
                            Layout.rightMargin: 7
                            Layout.topMargin: 15
                            Layout.bottomMargin: 5
                            //anchors.centerIn: parent.Center
                            width:32; height:32
                            source: iconPath
                        }
                        RowLayout {
                        Rectangle{
                            id: circleImgWO
                            Layout.leftMargin: 5
                            Layout.topMargin: 1
                            Layout.bottomMargin: 1
                            width: 11
                            height: width
                            radius: width/2
                            border.width: 2
                            border.color: "green"
                            color: "green"
                        }
                        Rectangle{
                            id: circleImgWth2
                            Layout.leftMargin: 1
                            Layout.topMargin: 1
                            Layout.bottomMargin: 1
                            //anchors.left: circleImgWth.right
                            width: 11
                            height: width
                            radius: width/2
                            border.width: 2
                            border.color: "green"
                            color: "transparent"
                        }
                        Rectangle{
                            id: circleImgWth
                            Layout.leftMargin: 1
                            Layout.topMargin: 1
                            Layout.bottomMargin: 1
                           // anchors.left: circleImgWO.right
                            width: 11
                            height: width
                            radius: width/2
                            border.width: 2
                            border.color: "green"
                            color: "transparent"
                        }
                        } }
                        ColumnLayout {
                            id: cl
                            Text {
                                text: name
                                visible: false
                            }
                            Text {
                                text: firstLanguage+'/'+secondLanguage
                                visible: false
                            }
                            Text {
                                id: firstLang
                                text: firstLanguage+': '+firstLangCaption
                            }
                            Rectangle {
                                id: firstLongDescRect
                                color: compRect.color
                                height:firstLangDesc.height
                                visible: moreLessVisibility
                                width: listView.width - 50 - buttonsLayout.width
                                radius: 3
                                border.width: 0.5
                                border.color: "blue"
//                                Layout.bottomMargin: 5
//                                Layout.rightMargin: 5
                                Text {
                                    id: firstLangDesc
                                    text: firstLangDescription
                                    anchors.left: parent.left
                                    width: parent.width
                                    wrapMode: Text.Wrap
                                    visible: moreLessVisibility
                                }
                            }
                            Text {
                                id: secondLang
                                text: secondLanguage+': '+secondLangCaption
                            }
                            Rectangle {
                                id: secondLongDescRect
                                color: compRect.color
                                height:secondLangDesc.height
                                visible: moreLessVisibility
                                width: listView.width - 50 - buttonsLayout.width
                                radius: 3
                                border.width: 0.5
                                border.color: "blue"
                                Layout.fillHeight: true
//                                Layout.bottomMargin: 5
//                                Layout.rightMargin: 5
                                Text {
                                    id: secondLangDesc
                                    text: secondLangDescription
                                    visible: moreLessVisibility
                                    anchors.left: parent.left
                                    width: parent.width
                                    wrapMode: Text.Wrap
                                }
                            }
                            Text {
                                text: iconPath
                                visible: false
                            }
                        }
                    }
                    ColumnLayout{
                        id: buttonsLayout
                        anchors.right: parent.right
                        Button {
                            id: showMoreLess
                            Layout.topMargin: 5
                            Layout.rightMargin: 5
                            text: qsTr("Show more")
                            leftPadding: 5
                            onClicked: {
                                listView.currentIndex = index;
                                print("showMoreLess Pressed")
                                moreLessVisibility = !moreLessVisibility
                            }
                        }
                        Button {
                            id: startButton
                            Layout.bottomMargin: 5
                            z: 100
                            text: qsTr("Start")
                            onClicked: {
                                listView.currentIndex = index;
                                print("cl"+cl.height, "rl"+rl.height, compRect.height, firstLongDescRect.height)
                            }
                        }
                    }

                }
            }
            Component {
                id: highlightBar
                Rectangle {
                    color: "lightsteelblue"
                    radius: 5
                    //y: listView.currentItem.y;
                    // #Behavior on y { SpringAnimation { spring: 2; damping: 0.1 } }
                }
            }
            delegate: listItem
            highlight:  Rectangle { color: "lightsteelblue"; radius: 5; }
            spacing: 3
            // highlightFollowsCurrentItem: true
        }
    }
    minimumWidth: 320
    minimumHeight: 240
}

