#ifndef LESSONPREVIEW_H
#define LESSONPREVIEW_H
#include <QFile>
#include <QPair>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QObject>

typedef struct LanguageDescription LanguageDescription;
struct LanguageDescription {
    QString language;
    QString caption;
    QString description;
};

class LessonPreview: public QObject
{
            Q_OBJECT
    Q_PROPERTY(QString name READ getName)
    Q_PROPERTY(QString version READ getVersion)
    Q_PROPERTY(QString firstLanguage READ getFirstLanguageName)
    Q_PROPERTY(QString firstLangCaption READ getCaptionFirst)
    Q_PROPERTY(QString firstLangDescription READ getDescriptionFirst)
    Q_PROPERTY(QString secondLanguage READ getSecondLanguageName)
    Q_PROPERTY(QString secondLangCaption READ getCaptionSecond)
    Q_PROPERTY(QString secondLangDescription READ getDescriptionSecond)
    Q_PROPERTY(QString numberOfWords READ getNum)
    Q_PROPERTY(QString iconPath READ getIcon)


public:
    LessonPreview(QString fileName, QObject *parent=0);
//    LessonPreview(const LessonPreview &copied);
    QString getName() const {
        return name;
    }
    QString getFirstLanguageName() {
        return firstLanguage.language;
    }
    QString getSecondLanguageName() {
        return  secondLanguage.language;
    }
    int getVersion() const {
        return version;
    }
    int getNum () const {
        return numberOfWords;
    }
    QString getIcon () const {
        return iconPath;
    }
    LanguageDescription getFirstLanguage () const {
        return firstLanguage;
    }
    QString getCaptionFirst () const {
        return firstLanguage.caption;
    }
    QString getDescriptionFirst () const {
        return firstLanguage.description;
    }
    QString getCaptionSecond () const {
        return secondLanguage.caption;
    }
    QString getDescriptionSecond () const {
        return secondLanguage.description;
    }
    LanguageDescription getSecondLanguage () const {
        return secondLanguage;
    }
private:
    QString name;
    //QPair <QString, QString> languages;
    int version;
    int numberOfWords;
    QString iconPath;
    LanguageDescription firstLanguage, secondLanguage;
};

#endif // LESSONPREVIEW_H
