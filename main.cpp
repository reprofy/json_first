#include "mainwindow.h"
#include <QApplication>
#include <QQmlApplicationEngine>
#include <QtQml>
#include <QDir>
#include <QStringList>
#include <QString>
#include "lessonpreview.h"
#include <iostream>

#include <QtQuick/qquickitem.h>
#include <QtQuick/qquickview.h>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QQmlApplicationEngine engine;
//    qmlRegisterType < LessonManager > ("lesson.myqt.org", 1, 0, "LessonManager");
    QList <QObject*> lessons;
    QDir dir("../json_first/resources");
    dir.setFilter(QDir::Files | QDir::Hidden | QDir::NoSymLinks);
    QStringList sl("*.json");
    dir.setNameFilters(sl);
    QFileInfoList list = dir.entryInfoList();
    for (int i=0; i < list.size(); i++) {
        QFileInfo fileInfo = list.at(i);
        qDebug() << fileInfo.filePath();
        QString newPath (fileInfo.filePath());
        lessons.append(new LessonPreview(newPath));
    }
 //   QQuickView dataView;
//    dataView.setResizeMode(QQuickView::SizeRootObjectToView);
    QQmlContext *ctxt=engine.rootContext();
    ctxt->setContextProperty("myModel", QVariant::fromValue(lessons));
//    dataView.setSource(QUrl("qrc:qml.qml"));
//    dataView.show();
    engine.load(QUrl(QStringLiteral("qrc:/qml.qml")));
    //qmlRegisterType < lessons > ("lesson.myqt.org", 1, 0, "LessonManager");
 //   LessonPreview first("/home/reprofy/Projects/qt_projects/json_first/json_first/kitchen_en_ru.json");
    return a.exec();
}
