#include "lessonpreview.h"
#include "QDebug"


LessonPreview::LessonPreview(QString fileName, QObject *parent):QObject(parent)
{
    QFile jsonData (fileName);
    if (!jsonData.open(QIODevice::ReadOnly)) {
        qWarning ("Couldn't open the file");
        exit(1);
    }
    QByteArray saveData = jsonData.readAll();
    QJsonDocument loadDoc (QJsonDocument::fromJson(saveData));
    QJsonObject lessonObject (loadDoc.object());
    name = lessonObject["name"].toString();
    version = lessonObject["appversion"].toInt();
    numberOfWords = lessonObject["number_of_words"].toInt();
    iconPath = lessonObject["icon_img"].toString();
    QFile icon (iconPath);
    if (!icon.open(QIODevice::ReadOnly))
        iconPath = "icon.png";
    QJsonObject firstLang = lessonObject["first_language"].toObject();
    firstLanguage.language = firstLang["language"].toString();
    firstLanguage.caption = firstLang["caption"].toString();
    firstLanguage.description = firstLang["description"].toString();
    qDebug() << firstLanguage.language << firstLanguage.caption << firstLanguage.description;
     QJsonObject secondLang = lessonObject["second_language"].toObject();
    secondLanguage.language = secondLang["language"].toString();
    secondLanguage.caption = secondLang["caption"].toString();
    secondLanguage.description = secondLang["description"].toString();
    qDebug() << secondLanguage.language << secondLanguage.caption << secondLanguage.description << version;
}
//LanguageDescription firstLanguage, secondLanguage;

/*
LessonPreview::LessonPreview(const LessonPreview& copied) {
    name = copied.getName();
    languages = copied.getLangs();
    version = copied.getVersion();
    numberOfWords = copied.getNum();
    iconPath = copied.getIcon();
    firstLanguage = copied.getFirstLanguage();
    secondLanguage = copied.getSecondLanguage();
}
*/
